# Javascript Warmup

### Instructions

1) Clone repo or download folders. 

2) Open each exercise file `ex-*.js` from corresponding folder `warmup-js-*` in your favorite IDE and coding your solution for challenges. Follow instructions, such as:

```javascript
/*
There're challenges where you have to write the solution in
three differents ways:  function declaration standard,
function expression standard and ES6 Standard. The three 
functions must be checked one by one and tests must be true.
*/

/**Example**/

/**
 * flipColor()
 *
 * Write a function called flipColor. This function may be
 * used to change the color of a tile in a game. It should
 * take as input an object. If that object's color property
 * has the value blue, it should change it to red, and
 * vice-versa.
*/


// write the function here (: function declaration standard


// write the function here (: function expression standard


// write the function here (: ES6 Standard



// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*


```

4) Every challenge has tests that must be checked, such as: 

```javascript

/**Example**/

/**
 * flipColor()
**/

// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
console.group('ex-02');
  console.log('%cFunction: flipColor', 'background-color: green; color: white')
console.groupEnd();

var tileObj = {
    width: "200px",
    height: "150px",
    color: "blue"
}

/* ----------------------- TEST-1  ----------------------- */
// Function takes object w/ `color` property with value of 'red'
//   returns object with `color` property with value of 'blue'
/* ------------------------------------------------------ */
console.log('TEST-1');

var tileObj2 = flipColor(tileObj)
console.assert(tileObj2.color === "red")

/* ----------------------- TEST-2  ----------------------- */
// Function takes object w/ `color` property with value of 'blue'
//   returns object with `color` property with value of 'red'
/* ------------------------------------------------------ */
console.log('TEST-2');

var tileObj3 = flipColor(tileObj2)
console.assert(tileObj3.color === "blue")


/* ----------------------- TEST-3  ----------------------- */
// Other properties are not changed
/* ------------------------------------------------------ */
console.log('TEST-3');

console.assert(tileObj3.width === "200px")
console.assert(tileObj3.height === "150px")

/* ----------------------- END  ----------------------- */
console.log('\n')

```

5) Test your work from browser console. Open 'index.html' from corresponding folder > Open console, right click > inspect or inspect element > choose console tab.

6) At first each test in console has an error, such as:

![](images/driver-code-red.png)

or

![](images/driver-code-reference-error-01.png)

or 

![](images/driver-code-reference-error.png)

7) Coding and test your work. Each test in console from browser must be true. It must not have any error, such as:

![](images/driver-code-green.png)



> All the fun is in the console.
